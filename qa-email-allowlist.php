<?php

/**
 * This file is part of the qa-email-allowlist plugin
 * Copyright (C) 2020  Christopher Spinrath
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}

class qa_email_allowlist {

	function expand_name($relname) {
		return 'plugin_' . str_replace('\\', '_', get_class($this)) . '_' . $relname;
	}

	function opt_list($relname) {
		$l = explode(',', qa_opt($this->expand_name($relname)));
		$l = array_map('trim', $l);
		$l = array_map('mb_strtolower', $l);

		return $l;
	}

	function filter_email(&$email, $olduser) {
		$domain = substr(strrchr($email, "@"), 1);

		$domain_allowlist = $this->opt_list('domains');

		if (in_array(mb_strtolower($domain), $domain_allowlist))
			return null;

		$email_allowlist = $this->opt_list('emails');

		if(in_array(mb_strtolower($email), $email_allowlist))
			return null;

		return 'email address is not permitted'; // bad
	}

	function admin_form(&$qa_content)
	{
		$saved = false;

		if (qa_clicked($this->expand_name('save_button'))) {
			qa_opt($this->expand_name('domains'), qa_post_text($this->expand_name('domains_field')));
			qa_opt($this->expand_name('emails'), qa_post_text($this->expand_name('emails_field')));

			$saved = true;
		}

		return array(
			'ok' => $saved ? 'email allowlist settings saved' : null,

			'fields' => array(
				array(
					'label' => 'allowlist domains:',
					'type' => 'text',
					'value' => qa_opt($this->expand_name('domains')),
					'tags' => 'NAME="' . $this->expand_name('domains_field') . '"',
				),

				array(
					'label' => 'allowlist emails:',
					'type' => 'text',
					'value' => qa_opt($this->expand_name('emails')),
					'tags' => 'NAME="' . $this->expand_name('emails_field') . '"',
				),
			),

			'buttons' => array(
				array(
					'label' => 'Save Changes',
					'tags' => 'NAME="' . $this->expand_name('save_button') . '"',
				),
			),
		);
	}
}
