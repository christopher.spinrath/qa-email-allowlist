[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://gitlab.com/christopher.spinrath/qa-email-allowlist/-/blob/main/LICENSE)
[![pipeline status](https://gitlab.com/christopher.spinrath/qa-email-allowlist/badges/main/pipeline.svg)](https://gitlab.com/christopher.spinrath/qa-email-allowlist/-/commits/main)

## About

qa-email-allowlist is a small [filter plugin](https://docs.question2answer.org/plugins/modules-filter/) for [Question2Answer](https://www.question2answer.org).
It restricts new registrations to a configurable set of email addresses.
It is possible to allow specific email addresses as well as email addresses with certain domains.

For instance, this plugin can be employed to limit new registrations to participants of a class.

## Install

1. Create a new `qa-email-allowlist` folder within the `qa-plugin` folder of your Question2Answer installation.
```
$ mkdir /path/to/q2a/qa-plugin/qa-email-allowlist
```
2. Copy the `*.php` files as well as `metadata.json` into the newly created folder.
```
$ cp *.php metadata.json /path/to/q2a/qa-plugin/qa-email-allowlist/
```

For details we refer to the [plugin documentation of Question2Answer](https://docs.question2answer.org/plugins/), specifically the "Directory structure" section.

## Usage

The plugin can be configured via the admin panel of Question2Answer.
